[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/lobi%2Ftest-binder-pyramid/master)
# single file pyramid app on binder

This is a test how a simpple single file pyramid example wors in binder.
Because the idea was to also share the code in jupyter notebook it is written in a
notebook file.